## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?:1268
* ¿Cuánto tiempo dura la captura?:12.810068 seg
* Aproximadamente, ¿cuántos paquetes se envían por segundo?:100 paquetes por segundo aproximadamente
* ¿Qué protocolos de nivel de red aparecen en la captura?:IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N):400
* Dirección IP de la máquina A:216.234.64.16
* Dirección IP de la máquina B:192.168.0.10
* Puerto UDP desde el que se envía el paquete:54550
* Puerto UDP hacia el que se envía el paquete:49154
* SSRC del paquete:0x31BE1E0E (834543118)
* Tipo de datos en el paquete RTP (según indica el campo correspondiente):ITU-T G.711 PCMU
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?:0
* ¿Cuántos paquetes hay en el flujo F?:626
* ¿El origen del flujo F es la máquina A o B?:A
* ¿Cuál es el puerto UDP de destino del flujo F?:49154
* ¿Cuántos segundos dura el flujo F?:12.49 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?:21.187 ms
* ¿Cuál es el jitter medio del flujo?:0.229065
* ¿Cuántos paquetes del flujo se han perdido?:0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?:0,5
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?:18635
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Pronto, con un skew de 13.845
* ¿Qué jitter se ha calculado para ese paquete?: 0.157713 ms
* ¿Qué timestamp tiene ese paquete?: 1769337483
* ¿Por qué número hexadecimal empieza sus datos de audio?:f
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`:mar 24 oct 2023 11:52:25 CEST
* Número total de paquetes en la captura:2647
* Duración total de la captura (en segundos):13.636165592
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?:450
* ¿Cuál es el SSRC del flujo?:0xbf553341
* ¿En qué momento (en ms) de la traza comienza el flujo?: 10.6368011064
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 24770
* ¿Cuál es el jitter medio del flujo?:0
* ¿Cuántos paquetes del flujo se han perdido?:0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura):
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?:
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?:
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?:
